#!/usr/bin/env python
# -*- coding: utf-8 -*-


# This file is part of pydic, a free digital correlation suite for computing strain fields
#
# Author :  - Damien ANDRE, SPCTS/ENSIL-ENSCI, Limoges France
#             <damien.andre@unilim.fr>
#
# Copyright (C) 2017 Damien ANDRE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from matplotlib import pyplot as plt
import numpy as np
import os
import cv2
import glob
import sys

from Tkinter import Tk, BOTH, StringVar, IntVar, Text
from ttk import Frame, Button, Style, Label, Entry, Progressbar, Checkbutton
from tkFileDialog import askdirectory
import tkMessageBox


# the correlation window size (it may be adjusted)
correl_window_size = (15, 15)

# param for correlation 
lk_params = dict(winSize  = correl_window_size, maxLevel = 10,
                 criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

# locate the pydic module and import it  <-- for debug
import imp#  <-- for debug
pydic = imp.load_source('pydic', 'C:\Users\launap03\Desktop\microtrack\pydic.py')# <-- for debug



def get_point2track(img_ref, number_of_point):
    h,w  = img_ref.shape[:2]
    area = [(w/4, h/4),(3*w/4, 3*h/4)]
    win  = correl_window_size
    crop_img = img_ref[w/4:3*w/4, h/4:3*h/4]
    point = cv2.goodFeaturesToTrack(crop_img, number_of_point, 0.01, 10)
    res = []
    for p in point :
        p[0][0] += h/4
        p[0][1] += w/4
        res.append(p[0])
    return np.array(res)





class MainGui(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)   
        self.parent = parent
        self.parent.wm_title("Track points and shift images")
        
        Label(self, text="Image directory ").grid(row=1,column=1)
        Button(self, text="Choose...", command=self.choose_img_dir).grid(row=1,column=2)
        self.entry_img_dir = Entry(self)
        #self.entry_img_dir.insert(0, "/home/dada/work/recherche/microscopie/Fissuration/50") # debug
        self.entry_img_dir.grid(row=1,column=3)

        Label(self, text="Result directory ").grid(row=2,column=1)
        Button(self, text="Choose...", command=self.choose_res_dir).grid(row=2,column=2)
        self.entry_res_dir = Entry(self)
        #self.entry_res_dir.insert(0, "/home/dada/work/recherche/microscopie/Fissuration/test1") # debug
        self.entry_res_dir.grid(row=2,column=3)

        Label(self, text="--------------------------------").grid(row=3,column=1,columnspan=3)

        self.debug = IntVar()
        Label(self, text="Display tracked points ").grid(row=4,column=1)
        Checkbutton(self, text="", variable=self.debug).grid(row=4,column=2)
        

        Label(self, text="Number of point to track ").grid(row=5,column=1)
        self.number_of_point_to_track = IntVar()
        self.number_of_point_to_track.set(50)
        Entry(self, textvariable=self.number_of_point_to_track).grid(row=5,column=2)

        Label(self, text="Error tolerance (%)").grid(row=6,column=1)
        self.tol_error = IntVar()
        self.tol_error.set(5)
        Entry(self, textvariable=self.tol_error).grid(row=6,column=2)


        Label(self, text="--------------------------------").grid(row=7,column=1,columnspan=3)
        
        self.label_progressbar = Label(self, text="")
        self.label_progressbar.grid(row=8,column=1)
        Button(self, text="Run...", command=self.run).grid(row=8,column=2)
        self.progressbar = Progressbar(self,orient ="horizontal", mode ="determinate")
        self.progressbar.grid(row=8,column=3)


        self.msg = Text(self, state="disabled")
        self.msg.grid(row=9,column=1, columnspan=3)
        Button(self, text="Quit", command=self.quit).grid(row=10,column=2)

    def quit(self):
        self.parent.quit()
        
    def choose_img_dir(self):
        path = askdirectory(title=r'Choose a directory that contains tif files')
        self.entry_img_dir.delete(0, 'end')
        self.entry_img_dir.insert(0, path)

    def choose_res_dir(self):
        path =  askdirectory(title=r'Choose a directory that contains tif files')
        self.entry_res_dir.delete(0, 'end')
        self.entry_res_dir.insert(0, path) 

    def run(self):
        
        
        # read image files
        img_dir = self.entry_img_dir.get()
        res_dir = self.entry_res_dir.get()
        if img_dir is "" or  res_dir is "":
            tkMessageBox.showinfo('Problem', 'Please choose valid directory first')
            return

        if os.path.isdir(res_dir) is False:
            os.mkdir(res_dir)
        

        os.chdir(img_dir)
        files = sorted(glob.glob("*.tif"))
        if len(files) == 0:
            tkMessageBox.showinfo('Info', 'No tiff image were found')
            return

        try:
            self.progressbar["maximum"] = len(files)-1
            self.progressbar["value"] = 0
            first_img      = cv2.equalizeHist(cv2.imread(files[0], 0))
            point_to_track = get_point2track(first_img, self.number_of_point_to_track.get())
            cv2.imwrite(res_dir + '/' + files[0], first_img)
            msg = ""
            error = []
            if self.debug.get() is 1:
                pydic.draw_opencv(first_img, point=point_to_track, text=files[0])

            for i in range(len(files)-1):    
                 image_ref = first_img
                 image_str = cv2.equalizeHist(cv2.imread(files[i+1], 0))
                 tracked_point, st, err = cv2.calcOpticalFlowPyrLK(image_ref, image_str, point_to_track, None, **lk_params)

                 msg += files[i+1] + " correl with error=" + str(np.min(err))+ " %\n"
                 error.append(np.min(err))

                 if self.debug.get() is 1:
                     pydic.draw_opencv(image_str, point=tracked_point, text=files[i+1])

                 # looking for tracking point with error lower than 9%
                 max_error = np.min(err) + float(self.tol_error.get())
                 index = [(idx,val)[0] for idx,val in enumerate(err) if val <= max_error]
                 val1 = []
                 val2 = []
                 for idx in index:
                     val1.append(tracked_point[idx])
                     val2.append(point_to_track[idx])
                 val1 = np.array(val1)
                 val2 = np.array(val2)

                 # compute and apply shift
                 shift = np.average(-val1 + val2, axis=0)

                 img = cv2.imread(files[i+1], 0)
                 rows,cols = img.shape
                 M = np.float32([[1, 0, shift[0]], [0, 1, shift[1]]])
                 dst = cv2.warpAffine(img,M,(cols,rows))

                 cv2.imwrite(res_dir + '/' + files[i+1], dst)
                 self.progressbar["value"] = i+1
                 self.label_progressbar["text"] = 'writing file '+ str(i+1) + '/' + str(len(files)-1)
                 self.update()

            error = np.array(error)
            msg += "---------------------------\n"
            msg += "mean error=" + str(np.average(err)) + " %\n"
            msg += "max  error=" + str(np.max(err))     + " %\n"
            msg += "min  error=" + str(np.min(err)) + " %\n"
            msg += "std  error=" + str(np.std(err)) + " %\n"

            self.msg.configure(state='normal')
            self.msg.delete('1.0', 'end')
            self.msg.insert('end', 'Done with sucess. Please find images in the "' + res_dir + '" directory.\n\nStatistic :\n' + msg)
            self.msg.configure(state='disabled')


        except Exception as e:
            tkMessageBox.showinfo('Problem', 'The program failed, please ensure that the chosen directory contains tif files with helios metadata. The system error message is "' + str(e) + '"')
            sys.exit(0)

         
def main():
    root = Tk()
    app = MainGui(root)
    app.pack()
    root.mainloop()


if __name__ == '__main__':
    main()  

