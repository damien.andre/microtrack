#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of pydic, a free digital correlation suite for computing strain fields
#
# Author :  - Damien ANDRE, SPCTS/ENSIL-ENSCI, Limoges France
#             <damien.andre@unilim.fr>
#
# Copyright (C) 2017 Damien ANDRE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from Tkinter import Tk, BOTH, StringVar, IntVar
from ttk import Frame, Button, Style, Label, Entry, Progressbar
from tkFileDialog import askdirectory
import tkMessageBox
import glob, os, sys
import tifffile as tiff
import shutil

def extract_metadata(file):
    metadata = ""
    with tiff.TiffFile(file) as tif:
        images = tif.asarray()
        for page in tif:
            for tag in page.tags.values():
                t = tag.name, tag.value
                if t[0] == 'helios_metadata':
                    metadata = t[1]
                image = page.asarray()

    # remove non wanted char in metadata 
    metadata = str(metadata)
    metadata = metadata.replace(" ", "")
    metadata = metadata.replace("*", "")

    # put metadata in a dictionnary
    d = {}
    for line in metadata.splitlines():
        s = line.split(':')
        if len(s) == 2:
            d[s[0]] = s[1]

    # and return the dict
    return d
    

class MainGui(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)   
        self.parent = parent
        self.parent.wm_title("Sort tif images")
        
        Label(self, text="Image directory: ").grid(row=1,column=1)
        Button(self, text="Choose...", command=self.choose_img_dir).grid(row=1,column=2)
        self.entry_img_dir = Entry(self)
        self.entry_img_dir.grid(row=1,column=3)


        Label(self, text="Result directory: ").grid(row=2,column=1)
        Button(self, text="Choose...", command=self.choose_res_dir).grid(row=2,column=2)
        self.entry_res_dir = Entry(self)
        self.entry_res_dir.grid(row=2,column=3)

        Label(self, text="").grid(row=3,column=1,columnspan=3)

        self.label_progressbar = Label(self, text="")
        self.label_progressbar.grid(row=4,column=1)
        Button(self, text="Run...", command=self.run).grid(row=4,column=2)
        self.progressbar = Progressbar(self,orient ="horizontal", mode ="determinate")
        self.progressbar.grid(row=4,column=3)

        Button(self, text="Quit", command=self.quit).grid(row=5,column=2)

    def choose_img_dir(self):
        path = askdirectory(title=r'Choose a directory that contains tif files')
        self.entry_img_dir.delete(0, 'end')
        self.entry_img_dir.insert(0, path) 

    def choose_res_dir(self):
        path =  askdirectory(title=r'Choose a directory that contains tif files')
        self.entry_res_dir.delete(0, 'end')
        self.entry_res_dir.insert(0, path) 
        
    def run(self):
        img_dir = self.entry_img_dir.get() 
        res_dir = self.entry_res_dir.get()

        if img_dir is "" or  res_dir is "":
            tkMessageBox.showinfo('Problem', 'Please choose valid directory first')
            return

        os.chdir(img_dir)
        files = glob.glob("*.tif")
        if len(files) == 0:
            tkMessageBox.showinfo('Info', 'No tiff image were found')
            return

        self.progressbar["maximum"] = len(files)
        self.progressbar["value"] = 0

        res = {}
        for i, f in enumerate(files):
            try:
                metadata = extract_metadata(f)
                
                # compute the image length in micrometer
                length = float(metadata['ResolutionX']) *  float(metadata['PixelWidth'])
                length = length*1e6
                # round int
                length = int(round(length/10.)*10.)
                
                dir_name = 'HW_' + str(length) + '_micron'

                # count file by new dir and create it if needed
                if dir_name in res.keys():
                    res[dir_name] += 1 
                else:
                    res[dir_name] = 1
                    new_dir_path = res_dir + '/' + dir_name
                    if os.path.isdir(new_dir_path) is False:
                        os.mkdir(new_dir_path)

                    
                shutil.copyfile(f, res_dir + '/' + dir_name + '/' + f)
                self.progressbar["value"] = i+1
                self.label_progressbar["text"] = 'copy file '+ str(i+1) + '/' + str(len(files))
                self.update()
                
            except Exception as e:
                tkMessageBox.showinfo('Problem', 'The program failed, please ensure that the chosen directory contains tif files with helios metadata. The system error message is "' + str(e) + '"')
                sys.exit(0)

        # display results
        msg = 'Process finished. Putting :\n'
        for key, value in res.items(): # returns the dictionary as a list of value pairs -- a tuple.
            msg += '- ' + key + ' images in the directory named "' + str(value) + '"\n' 
        tkMessageBox.showinfo('Sucess', msg)
        
    def quit(self):
        self.parent.quit()




def main():

    root = Tk()
    app = MainGui(root)
    app.pack()
    root.mainloop()


if __name__ == '__main__':
    main()  
